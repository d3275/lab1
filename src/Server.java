import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;

public class Server extends Thread {
    public static void main(String[] args) throws IOException {
        Config config = new Config();
        new ServerWorker(config.getPort()).start();
    }
}

class ServerWorker extends Thread {
    private int serverPort;
    private DatagramSocket socket;
    private HashMap<String, Integer> clients;

    public ServerWorker(int port) throws IOException {
        this.clients = new HashMap<>();
        this.serverPort = port;
        socket = new DatagramSocket(this.serverPort);
    }

    public void run() {
        while(true) {
            try {
                byte[] buffer = new byte[10000];
                DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
                socket.receive(incoming);

                String s = new String(incoming.getData(), 0, incoming.getLength());

                String command = s.split(":")[0];
                String responseString = "";
                int port = incoming.getPort();
                if (command.equals("login")) {
                    String payload = s.split(":")[1];
                    clients.put(payload, incoming.getPort());
                } else if (command.equals("list")) {
                    responseString = getClients();
                    InetAddress address = incoming.getAddress();
                    byte[] responseBytes = responseString.getBytes();
                    DatagramPacket response = new DatagramPacket(responseBytes, responseBytes.length, address, port);
                    socket.send(response);
                } else if (command.equals("sendMessage")) {
                    if(s.split(":").length > 2) {
                        responseString = s.split(":")[2];
                        String to = s.split(":")[1];
                        port = clients.get(to);
                        InetAddress address = incoming.getAddress();
                        byte[] responseBytes = responseString.getBytes();
                        DatagramPacket response = new DatagramPacket(responseBytes, responseBytes.length, address, port);
                        socket.send(response);
                    }
                }

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    private String getClients() {
        String repsonse = "";
        for (String client : clients.keySet()) {
            repsonse = repsonse + client + ",";
        }
        return repsonse;
    }
}
