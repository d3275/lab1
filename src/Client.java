import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Client {

    private static int port;
    private String name;

    public static void main(String[] args) throws SocketException {
        Config config = new Config();
        List<Client> clients = new ArrayList<Client>();
        DatagramSocket socket = new DatagramSocket();
        String message;
        try {
            InetAddress address = InetAddress.getByName("localhost");

            for (String clientName : args) {
                Client client = new Client(config.getPort(), clientName);
                clients.add(client);
                message = "login:" + client.getName();
                byte[] payload = message.getBytes();
                DatagramPacket packet = new DatagramPacket(payload, payload.length, address, port);

                socket.send(packet);
            }

            //get Clients
            message = "list:";
            byte[] listPayload = message.getBytes();
            DatagramPacket packet = new DatagramPacket(listPayload, listPayload.length, address, port);

            socket.send(packet);

            byte[] buf = new byte[1000];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            byte[] data = packet.getData();
            String list = new String(data, 0, data.length);

            ClientThreadWriter ctw = new ClientThreadWriter(socket);
            ctw.start();

            for (Client client : clients) {
                String[] registeredClients = list.split(",");
                for (String registeredClient : registeredClients) {
                    if (!registeredClient.trim().isEmpty() && !client.getName().equals(registeredClient)) {
                        message = String.format("sendMessage:%s:%s messaged %s", client.getName(), client.getName(), registeredClient);
                        byte[] payload = message.getBytes();
                        packet = new DatagramPacket(payload, payload.length, address, port);

                        socket.send(packet);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Client(int port, String name) throws SocketException {
        this.port = port;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}

class ClientThreadWriter extends Thread {
    DatagramSocket socket;

    public ClientThreadWriter(DatagramSocket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            while (true) {
                byte[] buf = new byte[10000];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);

                byte[] data = packet.getData();
                String message = new String(data, 0, data.length);

                System.out.println("MESSAGE: " + message);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}